\section{Related Work} 
\label{sec:related}
		
		Code cloning and its impact has a long history as a topic of inquiry
		in the research community.  In this section, we draw comparisons with
		existing research in this direction.
		
		Traditionally, researchers have discussed many negative effects of
		code cloning~\cite{johnson:94, fowler:99, lozano:07, krinke:07,
			shuai:2014, juergen:09}, however, the widespread presence of clones
		have motivated researchers to dig deeper and understand the usage
		scenarios.
		
		Kapser et al. presented eleven patterns by examining clones in two
		projects~\cite{kasper:2008}. They found out that not all usage
		patterns have negative consequence and some may even have positive
		consequence on quality.
		
		Ossher et al. looked at circumstances of file cloning in open source
		Java projects and classified the cloning scenarios into good, bad and
		ugly~\cite{Ossher:icsm11}. These scenarios included good use-cases
		like extension of Java classes and popular third-party libraries, both
		large and small. They also found ugly cases where a number of projects
		occur in multiple online repositories, or have been forked, or were
		copied and divided into multiple sub projects. From a software
		engineering standpoint, some of these situations are more legitimate
		than others.
		
		Kim et al. studied clone evolution in two open source projects. They
		found that most of the clone pairs are short lived and about 72\% of
		the clone pairs diverge within 8 commits in the code
		repository~\cite{kim:2005}. They found that several clones exist by
		design and cannot be refactored because of the limitation of
		programming language or it would require a design change. Similarly,
		de Wit et al. proposed CLONEBOARD, an Eclipse plug-in implementation
		to track live changes to clones and offering several resolution
		strategies for inconsistently modified
		clones~\cite{DeWit:2009.icsm}. They conducted a user study and found
		that developers see the added value of the tool but have strict
		requirements with respect to its usability.
		
		Cordy analyzed clones and intentions behind cloning of a financial
		institution system and argued that external business factors may
		facilitate cloning~\cite{cordy:03}. He mentioned that financial
		institutions avoid situations that can break the existing code under
		any circumstances. Abstractions might introduce dependencies and
		modifying such abstractions induces the risking of breaking existing
		code.
		
		Cloning minimizes this risk as code is maintained and modified
		separately localizing the risk of errors to a single
		module. Similarly, Rajapakse et al. found that reducing duplication in
		a web application only had negative effects on the modifiability of an
		application~\cite{rajapakse:07}. They noted that after significantly
		reducing the size of the source code, a single change required testing
		of a vastly larger portion of the project.
		
		Rahman et al. investigate the effect of cloning on defect proneness on
		four open source projects~\cite{rahman:2012}. They looked at the buggy
		changes and explored their relationship with cloning. They did not
		find evidence that cloned code is riskier than non cloned code. This
		finding is also supported by Sajnani et
		al.~\cite{sajnani2014comparative}, where they conducted an empirical
		study comparing the presence of bug patterns in cloned and non cloned
		code on 36 open source Java projects. Surprisingly, they found that
		the defect density of cloned code is less than 2 times than that of
		the non cloned code.
		
		Brutnik et al. use clone detection techniques in a novel way to find
		cross-cutting concerns in the code~\cite{Bruntink:2005.tse}. They
		manually identify five specific cross-cutting concerns in an
		industrial C project and analyze to what extent clone detection is
		capable of finding them. The initial results were positive.
		
		Thus, evaluating the positive and negative impacts of cloning has been
		a continuous balancing act. Like some of the recent studies, our study
		also presents one more piece of evidence that cloning may not be that
		bad after all. However, our study differs from similar studies in the
		literature in several aspects.
		
		We explore the relationship between a set of 27 well known software
		metrics across three categories and code clones, and compare and
		contrast the nature of this relationship in non cloned code. To the
		best of our knowledge, no other study has investigated code cloning
		using such a comprehensive set of metrics.  These metrics are a
		proactive way of looking at the patterns which are potential threats
		for the code base. Other studies have looked at the relationship of
		bugs with cloning by mining the version control systems to find out
		the bugs associated with clones. While such reported bugs provide a
		measure of external quality of the overall project, researchers have
		identified several issues with respect to completeness and correctness
		of such data. Moreover, like all complex problems, the issue of code
		cloning being bad or not will only be fully understood by looking at
		it from several angles and with several methodologies. Moreover,
		several studies have also evaluated the effectiveness of software
		quality metric systems and found it to be a useful indicator of code
		quality~\cite{Basili:1996nx,kafura87, Li93}.
		
		
		We conduct this study on 4,421 open source Java projects. Our study is the first to perform clone-quality analysis at this level. This scale not
		only helps us to instill more confidence in the findings of the study, but also helps to discover and hence account for confounding factors
		that might impact the validity of the study. The findings of this study not only help in understanding the nature of code clones, but also help in identifying hot spots which are more likely to be re-used in the future. 