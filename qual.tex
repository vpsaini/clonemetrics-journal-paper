\section{Qualitative Analysis}
\label{sec:qual}

To understand why the cloned methods don't seem to differ much from
the non cloned methods, except on size, we conducted a qualitative
analysis. This analysis was made in three parts. We first looked at a
small subset of cloned and non-cloned methods in search for validation
of the quantitative results, as well as patterns and correlations
among different characteristics of the methods. On a second step, we
looked at pairs of clone methods. Finally, we looked at groups of
cloned methods. This section describes our qualitative analysis and
findings.
		
\subsection{Analysis of Methods}
\label{sec:analysismethods}

From a total set of 644,830 cloned methods and 842,052 non cloned
methods we created two sets of 150 randomly selected cloned methods
and 150 randomly selected non-cloned methods. Each author then
inspected 50 cloned and 50 non-cloned methods. After analyzing the
methods, we consolidated our findings.
		
The analysis was made along four dimensions, namely \textit{Size},
\textit{Comprehensibility}, \textit{Usability of comments}, and
\textit{Functionality}. We describe the analysis and findings below.
		
\subsubsection{Size}

We wanted to understand whether a method looks very small,
medium, large, or very large to the observers. The Likert Scale uses
options 1 to 5, where the size increases as we move from option 1 to
option 5. Columns 2 to 6 in Table~\ref{tab:size} correspond to each
of these options. Column \textit{Type} contains the type of the
methods analyzed; column \textit{VS}, which corresponds to option 1 in
the Likert scale, shows the number of the methods that were very small
(3-8 \textit{NLOC}); column \textit{S}, (option 2) shows the number of
methods that small (9-20 \textit{NLOC}); column \textit{M} (option 3)
contains the number of methods that were of medium size (21-40
\textit{NLOC}); column \textit{L} (option 4) shows the number of
methods that were large (41-70 \textit{NLOC}); and column \textit{VL}
(option 5) shows the number of methods that were very large in size
($>$70 \textit{NLOC}).
	
\begin{table}
\begin{center}
\begin{tabular}{l*{6}{c}r}
Type      & VS & S & M & L & VL \\ \hline
Clone     & 33 & 71 & 25 & 12 & 9 \\
Non-Clone & 30 & 54 & 42 & 10 & 14\\
\end{tabular}
\caption{Size of cloned and non cloned methods}
\label{tab:size}
\end{center}
\end{table}

	
For size, we observed a noticeable difference in the number of methods
classified as small(\textit{S}) and very small (\textit{VS}), with cloned
methods being more often in these two categories. Conversely,
non-cloned methods were more often found to be very
large (\textit{VL}). The observation confirms our findings in the
quantitative study -- that there are more smaller methods in the set
of clones.
	
Interestingly, we also noticed that in three of the cloned methods in
our samples there were no method bodies but only method signatures,
meaning that these methods came from interfaces. However, they passed
the threshold for analysis, because they have more than 25 tokens --
these are methods with many parameters of complicated types. We note
that we had removed all methods with less than 25 tokens to ignore
empty methods, stubs, method contracts defined in interfaces, abstract
classes and other interferences. The heuristic did remove a many
insignificant methods, but not all of them, as seen
here. Listing~\ref{lst:interfacemethod} shows an example of one such
method signature. 
	
	\begin{lstlisting}[language=Java, label={lst:interfacemethod},caption={Example of an empty method found during the qualitative study},frame=tlrb,
	basicstyle=\scriptsize, %or \small or \footnotesize etc.
	columns=flexible,breaklines=true,breakatwhitespace=true
	]
	
	@Transactional(propagation = Propagation.SUPPORTS, readOnly = true)
	public com.liferay.portal.model.PersistedModel getPersistedModel(
	java.io.Serializable primaryKeyObj)
	throws com.liferay.portal.kernel.exception.PortalException,
	com.liferay.portal.kernel.exception.SystemException;
	\end{lstlisting}

Upon further investigation, we found that for such methods JHawk
calculates \textit{NOS} as 1. There are 15,147 out of 644,830 cloned
methods (2.3\%) and 4,568 out of 842,052 non cloned methods (0.5\%) in
our dataset with \textit{NOS}$=$1. The presence of such methods could
partially explain the size difference between cloned and non cloned
methods. This is an interesting insight that could not have been
captured in the quantitative analysis of the previous section.

\subsubsection{Comprehensibility} 

We capture the comprehensibility of a method using a Likert Scale
ranging from 1 to 5, where a score of 1 means that the method is very
easy to understand and 5 indicates that the method is very
difficult to understand. This is an entirely subjective judgement,
which is based on many variables that are not controlled for, including
our own experience. Nevertheless, we believe this exercise was worth
doing, because it would allow us to detect patterns, if they were to
exist. For example, would the cloned methods appear to be simpler than
the non-cloned methods?

Comprehensiblity here is loosely related to the complexity metrics of
the previous section, but it's not the same. Code may be hard to
understand even when the logic is very simple -- e.g. when the
variable names are incomprehensible or the reader is not familiar with
the used APIs.

\begin{table}
\begin{center}
\begin{tabular}{l*{6}{c}r}
Type      & VE & E & M & H & VH \\ \hline
Clone     & 31 & 65 & 28 & 13 & 13 \\
Non-Clone & 32 & 69 & 32 & 10 &  7\\
\end{tabular}
\caption{Comprehensibility of cloned and non cloned methods}
\label{tab:comprehensibility}
\end{center}
\end{table}

Table~\ref{tab:comprehensibility} shows the results. Column
\textit{Type} contains the type of the methods analyzed; column
\textit{VE}, which corresponds to option 1 in the Likert scale, shows
the number of the methods that were very easy to understand; column
\textit{E}, (option 2) shows the number of methods that were easy to
understand; column \textit{M} (option 3) contains the number of
methods that were neither easy nor hard to understand; column
\textit{H} (option 4) shows the number of methods that were hard to
understand; and column \textit{VH} (option 5) shows the number of
methods that were very hard to understand.

The distributions of both types of methods are similar, indicating
that there were no clear differences between cloned and non-cloned
methods in terms of their comprehensibility. We note, however, that we
classified more cloned methods in the VH category (13) than non-cloned
methods (7) in that same category. This was  somewhat surprising;
our additional investigations may shed light into why this may be
happening. 
		
Listing~\ref{lst:veasymethod} shows a small sized method which was
classified as very easy to understand. The method uses familiar
variable names and performs a task of calling \textit{appendFiled}
method. The method shown in Listing~\ref{lst:vhardmethod} is an
example of a method classified as very hard to understand. The method
intends to set the \textit{value} of \textit{att} variable for which
it calls \textit{Super} at the end. Before that, it uses nested
if statements, and uses a variable \textit{\_att\_concise} which is
neither defined in the method nor passed to the method, making it
difficult to know its type. Also, it throws a
\textit{RuntimeException} from the inner if statement, adding to the
difficulty in code comprehension.
	
	\begin{lstlisting}[language=Java, label={lst:veasymethod},caption={Example of a very easy to understand method},frame=tlrb,
	basicstyle=\scriptsize, %or \small or \footnotesize etc.
	columns=flexible,breaklines=true,breakatwhitespace=true
	]
	public StringBuilder appendFields(ObjectLocator locator, StringBuilder buffer, ToStringStrategy strategy) {
	  {
	    List<Format> theFormat;
	    theFormat = this.getFormat();
	    strategy.appendField(locator, this, "format", buffer, theFormat);
	  }
	  {
	    List<DCPType> theDCPType;
	    theDCPType = this.getDCPType();
	    strategy.appendField(locator, this, "dcpType", buffer, theDCPType);
	  }
	  return buffer;
	}
	\end{lstlisting}
	
	
	\begin{lstlisting}[language=Java, label={lst:vhardmethod},caption={Example of a very hard to understand method},frame=tlrb,
	basicstyle=\scriptsize, %or \small or \footnotesize etc.
	columns=flexible,breaklines=true,breakatwhitespace=true
	]
	public void setConcise(String value) throws RuntimeException {
	  StringSTAttribute att = null;
	  if (_att_concise == null) {
	    _att_concise = (StringSTAttribute) attributeFactory.getAttribute("concise", "formula");
	    if (_att_concise == null) {
	      throw new RuntimeException("BUG: cannot process attributeGroupName : concise probably incompatible attributeGroupName and attributeName");
	    }
	  }
	  att = new StringSTAttribute(_att_concise);
	  super.addRemove(att, value);
	}
	\end{lstlisting}
	
Not surprisingly, we observed some positive correlation between the size
of the methods and their comprehensibility. Most of the methods that
were hard to understand were also bigger in size. However, we found
some methods that were small and yet hard to understand, such as the
one shown in Listing~\ref{lst:vhardmethod}.
	
\subsubsection{Usability of Comments}
	
We wanted to understand whether the provided comments are helpful in
the comprehensibility of the methods, and whether there were
differences in the comments of cloned vs. non cloned methods. As such, we captured the
subjective usefulness of the comments using a Likert Scale between 1
and 4, where the usability of comments increases as we move from
option 1 to option 4. This is related to the comments metrics of the
quantitative analysis, but it's not the same. Specifically, the
quantitative metrics cannnot capture whether a comment is useful or
not; only people can do that.

\begin{table}
\begin{center}
\begin{tabular}{l*{5}{c}r}
Type      & NC & JC & UC & VUC \\ \hline
Clone     & 90 & 7 & 39 & 14  \\
Non-Clone & 76 & 3 & 49 & 22 \\
\end{tabular}
\caption{Usability of comments on cloned and non cloned methods}
\label{tab:comments}
\end{center}
\end{table}
 
Columns 2 to 6 in Table~\ref{tab:comments}
correspond to each of these options. Column \textit{Type} contains the
type of the methods analyzed; column \textit{NC}, which corresponds to
option 1 in the Likert scale, shows the number of methods which had no
comments; column \textit{JC}, (option 2) shows the number of methods
which have mostly junk comments or commented out code or comments that
were practically useless; column \textit{UC} (option 3) contains the
number of methods which have somewhat useful comments. These comments
do help in the code comprehension or to understand the purpose of
given methods; column \textit{VUC} (option 4) shows the number of
methods which have very useful comments.
	
The difference between the quality of comments is noticeable between
cloned and non cloned methods. There are 90 methods for which clones
have no comments, whereas there are only 76 non cloned methods with no
comments. Moreover, the number of somewhat useful and extremely
useful comments for non cloned method are also larger than for the
cloned methods. When we looked into whether there is any correlation
between the quality of comments and the size of the methods, we found
no statistically significant correlation.
	
Listing~\ref{lst:EUCOM} shows and example of very useful comments. The comments in this method not only help to understand what the code is supposed to do, but also help to understand the rationale behind the decisions made in the code.
	
	\begin{lstlisting}[language=Java, label={lst:EUCOM},caption={A method with extremely useful comments},frame=tlrb,
	basicstyle=\scriptsize, %or \small or \footnotesize etc.
	columns=flexible,breaklines=true,breakatwhitespace=true
	]
	/**
	* Execute the Mojo.
	* <p>
	* The metadata model is loaded, and the specified template is executed with
	* any template output being written to the specified output file.
	* </p>
	*/
	public void execute() throws MojoExecutionException
	{
	  try
	  {
	    if (modelIds == null)
	    {
	      modelIds = new ArrayList();
	      modelIds.add(project.getArtifactId());
	    }
	    // Load the metadata file from an xml file (presumably generated
	    // by an earlier execution of the build-metadata goal.
	    Model model = IOUtils.loadModel(new File(buildDirectory, metadataFile));
	    // Flatten the model so that the template can access every property
	    // of each model item directly, even when the property is actually
	    // defined on an ancestor class or interface.
	    new Flattener(model).flatten();
	    generateConfigFromVelocity(model);
	  }
	  catch (IOException e)
	  {
	    throw new MojoExecutionException("Error during config generation", e);
	  }
	  catch (BuildException e)
	  {
	    throw new MojoExecutionException("Error during config generation", e);
	  }
	}
	\end{lstlisting} 
	
Listing~\ref{lst:JUNK_COM} shows a method with \textit{Javadoc} comments. These comments are practically useless for a developer who wishes to understand the code. 
	
	\begin{lstlisting}[language=Java, label={lst:JUNK_COM},caption={A method with junk comments},frame=tlrb,
	basicstyle=\scriptsize, %or \small or \footnotesize etc.
	columns=flexible,breaklines=true,breakatwhitespace=true
	]
	/**
	* @param jsonValues
	* @return
	* @throws JSONException
	*/
	private static long[] convertJSONToLongArray(JSONArray jsonArray) throws JSONException
	{
	  long[] longArray = new long[jsonArray.length()];
	  if (jsonArray != null && jsonArray.length() > 0)
	  {
	    for (int i = 0; i < jsonArray.length(); i++)
	    {
	      longArray[i] = jsonArray.getLong(i);
	    }
	  }
	  return longArray;
	}
	\end{lstlisting} 
	
\subsubsection{Functionality} 

We wanted to understand whether cloned and non-cloned methods were
doing fundamentally different things. As such, we analyzed these
methods in terms of the functionality they seem to be implementing.
	
After a first pass, we tagged each method with our best guess of the
functionality that they seem to implement. This exercise took around 5
hours per person to tag 100 methods each. After the tagging was done,
we went through all the tags to create a set of normalized tags. To
understand the normalization process, consider an example, a list
containinig the following non-normalized tags: \textit{“Sorting”, “Sort”,
  “parsing”, “parser”, “parse”}.  To normalize this list, we replaced
semantically similar tags with a common tag. After the normalization
of tags, the methods were re-tagged with the normalized tags by
replacing the original tag with their normalized counterpart.

We note that this process of tagging methods had some challenges. In
many cases it was difficult to come up with an exclusive tag that fully
captured the functionality of the method because of several factors --
size of methods (bigger methods usually tend to do more than one
thing, making it hard to associate them with one functionality);
complex application specific logic; lack of context; among
others. Also, there is no fixed set of functionalities that we could
have used in tagging. Nevertheless, we believe this exercise was
important, as it captured some of the essence of what we observed in
the methods.  The list of normalized tags along with their description
is as follows.

\begin{itemize}
	
	\item \textit{AppLogic}, a method that usually performs multiple responsibilities, often manifested in the form of two or more method calls.
	
	\item \textit{Wrapper}, a method that often validates certain pre-conditions, transforms the parameters (e.g. objects) to create parameters for another method call with a similar responsibility as the caller.
	
	\item \textit{Iterator} a method that iterates over a collection and executes one or more statements in a loop.
	
	\item \textit{Conditions}, a method that uses a series of if else or switch case conditions to do the main task.
	
	\item \textit{Setter}, a method that sets a value of a class attribute.
	
	\item \textit{Getter}, a method that return a value of a class attribute.
	
	\item \textit{Parser}, a method that transforms a piece of
          code (e.g. String) based on grammer rules into another form
          (e.g. AST). Lexers are also included in this tag.
	
	\item \textit{StringBuilder}, a method that builds a string, usually by calling append many times.
	
	\item \textit{Math}, a method that takes numbers as input and performs a mathematical operation with them.
	
	\item \textit{Pattern Matching}, a method that matches a pattern in a string.
	
	\item \textit{Serializer}, a method that transforms an Object
          into string or bytes, or vice-versa.
	
	\item \textit{Clone}, a method that clones another object.
	
	\item \textit{Equals}, a method that overrides \emph{equals} method.
	
	\item \textit{HashCode}, a method that overrides \emph{HashCode} method.
	
	\item \textit{Testcase}, a method representing a unit test case to test a scenario.
		
	\item \textit{Logger}, a method whose main responsibility is to log execution details.
	
	\item \textit{FileIO}, a method whose main responsibility is to write/read to/from a file.
	
	\item \textit{Socket}, a method which uses sockets to perform networking tasks.
	
	\item \textit{NA}, methods which are usually overloaded with a series of very complex tasks and it's hard to classify them in any specific bucket of functionality. 
	
\end{itemize}

Figure~\ref{fig:featuretags} shows the bar charts for clone and non
cloned methods showing their distribution across functionality
tags. We observe that many cloned and non cloned methods are tagged
with \textit{AppLogic}, \textit{Wrapper}, \textit{Iterator},
\textit{Conditions}, \textit{Getter}, \textit{Setter},
\textit{Parser}, and \textit{StringBuilders}. In most of these tags,
except for \textit{Wrapper}, \textit{Parser}, and
\textit{StringBuilder}, there are more non cloned methods
than cloned methods. The observation indicates that although cloning
is exists in most of the functionalities, there exists some
functionalities where methods have much structural similarities to be
deemed clones.

\begin{figure}
	\centering
	\includegraphics[width=13cm]{plots/qualitative/featuretags.png}
	\caption {\scriptsize Distribution of cloned and non cloned
          methods across functionality tags. The Horizontal-axis shows the functionality tags and the Vertical-axis shows the number of methods in each functionality tag.}
	\label{fig:featuretags}
	\vspace{-5mm}
\end{figure}
	
\input{deepanalysis}

