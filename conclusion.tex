\section{Conclusion and Future Work}
		\label{sec:conclusion}
		
		We conducted an statistical study to explore if there exists any
		difference between the quality of cloned methods and non cloned
		methods, as measured by well-known software quality metrics. The
		dataset consists of 4,421 open source Java projects containing 644,830
		cloned and 842,052 non cloned methods. The study uses 27 software
		metrics, as a proxy for quality, spanning across complexity,
		modularity, and documentation categories.
		
		Statistically we find no evidence that the cloned methods are
		different from the non cloned methods for most of the metrics. The
		main exception pertained to size: we found that cloned methods, on an
		average (median \textit{NOS}), are 20\% smaller than the non cloned
		methods, which may indicate that developers are careful to copy-paste
		only small(er) pieces of code. A small fraction of the projects show
		statistically significant difference in the quality of cloned and
		non cloned methods, when controlled for size. There, however, are only
		three metrics \textit{NEXP}, \textit{HEFF}, and \textit{XMET} where
		the difference between the percentage of projects showing that the
		cloned methods have better metrics values and the percentage of
		projects showing that the non cloned methods have better metrics is at
		least 30\%. Also, our
		observations resonate with other studies in finding that size is
		a confounding variable in such studies. Similar to what is described
		in~\cite{lopes2015scale}, normalizing the metrics for size does not
		always make the metrics completely independent of size. 
		
		We performed a detailed qualitative analysis to understand why the cloned methods don't seem to differ much from
		the non cloned methods, except on size, we conducted a qualitative
		analysis. The qualitative analysis reinforced the quantitative findings, and it allowed us to gain some additional insights as to \textit{why} cloned methods in our dataset are smaller than non-cloned methods. We found two possible factors that may lead to the size differences: i) presence of many interface methods that have no body; and ii) presence of many small auto generated methods that are deemed clones because of their similar structure. Additionally, we gained some more insights about cloned
		methods. Specifically, among the clones that don't seem to be
		automatically generated, we found many instances of dual-purpose
		methods, i.e. methods that have very similar structure and vocabulary
		but do the opposite (expand vs. collapse, etc.). We also found that
		certain functionality tends to be more pervasive in cloned methods than non-cloned methods.
		
		%		Our findings resonate with some of the recent studies, and
		%		provide one more piece of evidence that cloning may not
		%		always be harmful. All of these studies including ours, suggest
		%		that the issue of code cloning is a complex one and there are
		%		several pros and cons to it. While code cloning has it side effects
		%		- code bloat, inconsistent changes, breaking abstractions,
		%		etc., it is also evident that the practice of code cloning has
		%		become an integral part of our software development process.
		%		Perhaps it is an opportunity to focus on tools and techniques
		%		that help developers take advantage of rapid development using
		%		cloning and also manage clones effectively (and automatically)
		%		to counter the known side effects of code cloning.
		
		
		There are a number of directions for future work suggested by the
		results and limitations of our study. First, the set of software
		quality metrics could be further expanded to capture additional
		characteristics of cloned and non cloned methods.
		
		Including other source code repositories into the study is another
		area for future work. While the Maven repository represents a large fraction of the
		open source Java community, additional open source repositories like
		SourceForge, Google Code, and Apache would add greater diversity to
		the dataset. Especially of interest are those repositories based on
		more recent version control projects like Git and Mercurial, which
		have seen a surge in popularity in recent years. GitHub and Bitbucket
		are prime candidates, as they now hosts many popular open source
		projects. Another longer term goal is to expand the dataset with
		projects written in programming languages in addition to Java.
		
		The data collected for our study can also be used in numerous ways, in
		addition to further analysis of the primary question. For example, our
		extremely large collection of computed software quality metrics and
		the output of clone detector, which could be used to study the
		evolution of metrics in both cloned and non cloned
		methods. Additionally, the software quality data could be produced at
		different granularities such as class level, package level, and
		project level, which could be associated with open source community
		data to investigate if any metrics are related to properties of the
		surrounding open source community. Moreover, the software quality data
		at different granularities and at various versions of a project could
		be used to study the project evolution from different view points.
		
		\textbf{Dataset accessibility and Reproducibility}: We welcome other
		researchers to reproduce and even replicate the study on other
		software projects. For this, we have made available all the necessary
		artifacts including raw data, detailed steps and scripts to process
		data, and analysis procedure to reproduce the statistical results to
		verify the claims. All artifacts are made available at the following
		URL: http://mondego.ics.uci.edu/projects/clone-metrics.\\
		