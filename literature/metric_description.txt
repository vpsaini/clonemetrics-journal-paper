http://www.virtualmachinery.com/jhawkmetricslist.htm

Name	The name of the method
Cyclomatic Complexity	McCabes cyclomatic Complexity for the method
Number of Arguments	Number of Arguments
Number of Comments	The number of Comments associated with the method (See our discussion on this metric here)
Number of Comment Lines	The number of Comment Lines associated with the method (See our discussion on this metric here)
Variable Declarations	The number of variables declared in the method
Variable References	The number of variables referenced in the method
Number of statements	The number of statements in the method (See our discussion on this metric here)
Number of expressions	The number of expressions in the method (See our discussion on this metric here)
Max depth of nesting	The maximum depth of nesting in the method
Halstead length	The Halstead length of the metric (See our discussion on the Halstead Metrics here).
Halstead vocabulary	The Halstead vocabulary of the method (See our discussion on the Halstead Metrics here).
Halstead volume	The Halstead volume of the method (See our discussion on the Halstead Metrics here).
Halstead difficulty	The Halstead difficulty of the method (See our discussion on the Halstead Metrics here).
Halstead effort	The Halstead effort of the method (See our discussion on the Halstead Metrics here).
Halstead bugs	The Halstead prediction of the number of bugs in the method (See our discussion on the Halstead Metrics here).
Total depth of nesting	The total depth of nesting in the method
Number of casts	The number of class casts in the method
Number of loops	The number of loops (for, while) in the method
Number of operators	The total number of operators in the method
Number of operands	The total number of operands in the method
Class References	The classes referenced in the method
External methods	The external methods called by the method
Local methods	The local methods called by the method
Exceptions referenced	The exceptions referenced by the method
Exceptions thrown	The exceptions thrown by the method
Modifiers	The modifiers (static, public etc) applied to the signature of the method
Lines of Code	The number of lines of code in the method (See our discussion on this metric here)
